module multi(
				clock,
				reset,
				start,
				ready,
				ready1,
				ks,
				kd,
				left,
				right,
				LmR,
				LpR
			);
			
	parameter
			N=5,
			M=18;
			
	input clock;
	input reset;
	input start;
	output ready;
	output ready1;
	input unsigned [N-2:0] ks;
	input unsigned [N-2:0] kd;
	input signed [17:0] left;
	input signed [17:0] right;
	output reg signed [M+N-1:0] LpR;
	output reg signed [M+N-1:0] LmR;
	wire signed [M+N-1:0] preLpR;
	wire signed [M+N-1:0] preLmR;
	wire signed [N-1:0] Ks;
	wire signed [N-1:0] Kd;
	wire signed [17:0] rp;
	wire signed [17:0] rm;
	
	assign Ks = {{1'b0},{ks}};
	assign Kd = {{1'b0},{kd}};
	assign rp [17:0] = left[17:0] + right[17:0];
	assign rm [17:0] = left[17:0] - right[17:0];
		
	seqmultNM #(
				.N(N),
				.M(M)
	          ) 
			  seqmult_1
			  (
			  	.clock(clock),
			  	.reset(reset),
			  	.ready(ready),
			  	.start(start),
			  	.A(rp),
			  	.B(Ks),
			  	.R(preLpR)
			  );
	
	seqmultNM #(
				.N(N),
				.M(M)
	          ) 
			  seqmult_2
			  (
			  	.clock(clock),
			  	.reset(reset),
			  	.ready(ready1),
			  	.start(start),
			  	.A(rm),
			  	.B(Kd),
			  	.R(preLmR)
			  );
		
	always @(posedge ready)
	begin
		LpR<=preLpR[M+N-1:0];
	end
	
	always @(posedge ready1)
	begin
		LmR<=preLmR[M+N-1:0];
	end
	
endmodule

			  
	
