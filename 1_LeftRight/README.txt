-Só estavamos a fazer 12000 operações porque tinhamos
 2 ciclos for com a mesma variavel (i), por isso o i
 chegava a 24000 em metade do tempo que era suposto
 	-Solução: meter um dos for's com outra variavel
 	 "j".

-Encontrei/mos o ficheiro com os clock enables, por 
 isso passamos a usar o clken48kHz em vez do clock
 
-Adicionei um $stop ou um $finish para para a simulaçao
mas nem sempre funciona?

-Arranjei o script de python: agora funciona em comple-
 mento para 2.

-Para correr isto:
	0) mudar o endereço das entradas e das saidas
	1) criar o projeto no modelsim com os 3 ficheiros .v
	2) correr l3.py - mete o resultado final em decimal no
	   ficheiro L.txt
	3) correr l4.py - passa o resultado final para hex no 
	   ficheiro ke.txt - podes comparar isto com o 
	   fmstereo_audioin_left.hex, eles são iguais, ou pelo
	   menos deviam
	    
	
