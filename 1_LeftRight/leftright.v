module leftright(
					input signed [17:0] left,
					input signed [17:0] right,
					output reg signed [17:0] LpR,
					output reg signed [17:0] LmR,
					input clock,
					input reset
				);
		
		wire [17:0] rp;
		wire [17:0] rm;
				
		assign rp [17:0] = left [17:0] + right [17:0];
		assign rm [17:0] = left [17:0] - right [17:0];
		
		always @*
		begin
			LpR<=rp[17:0];
			LmR<=rm[17:0];
		end		
		
endmodule
