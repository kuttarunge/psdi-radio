`timescale 1ns / 1ps


module k48(

	input clock,
	input reset,
	input signed [17:0] LEFTin,
	input signed [17:0] RIGHTin,
	input [3:0] kd,
	input [3:0] ks,
	input clken48kHz,
	output reg signed [17:0] outLpR,
	output reg signed [17:0] outLmR
 );

	wire readyKS;
	wire readyKD;
	reg  signed [18:0] soma;
	reg  signed [18:0] sub;
	wire signed [17:0] LpR;
	wire signed [17:0] LmR;
	wire signed [22:0] LpRxKs;
	wire signed [22:0] LmRxKd;
	wire signed [4:0] Ks;
	wire signed [4:0] Kd;

	seqmultNM #(
				.M(18),
				.N(5)
			   ) 
		seqmult_1
		(
		.clock(clock),
		.reset(reset),
		.start(clken48kHz),
		.ready(readyKS),
		.A(LpR),
		.B(Ks),
		.R(LpRxKs)
	);

	seqmultNM #(
				.M(18), 
				.N(5)
				) 
		seqmult_2
		(
		.clock(clock),
		.reset(reset),
		.start(clken48kHz),
		.ready(readyKD),
		.A(LmR),
		.B(Kd),
		.R(LmRxKd)
		);

	always @(negedge clock)
	begin
		if (clken48kHz)
		begin
			soma = (LEFTin + RIGHTin);
			sub = (LEFTin - RIGHTin);
		end
	end

	assign LpR = soma[18:1];
	assign LmR = sub[18:1];
	assign Ks = {1'b0, ks};
	assign Kd = {1'b0, kd};


	always @(posedge clock)
	begin
		if (reset) 
		begin
		   sub <=0;
		   soma <=0;
		   outLpR <= 0;
		   outLmR <= 0;
		end
	end

	always @(posedge readyKS)
	begin
		outLpR <= LpRxKs[22:3];
	end

	always @(posedge readyKD)
	begin
		outLmR <= LmRxKd[22:3];
	end

endmodule





