`timescale 1ns / 1ps

module dds(
			clock,
			reset,
			enableclk,
			phaseinc,
			outsine
			);
			
	parameter Nsamples_LUT =64,
			  N=6,
			  n=12,
			  on=8,
			  DDSLUT ="DDSLUT.hex",
			  DDSLUTo="DDSout.hex",
			  NBITS = 32;

	
	input clock;
	input reset;
	input enableclk;
	input [N+n-1:0] phaseinc;
	output reg [on-1:0] outsine;
	
	reg [N+n-1:0] pplus;
	reg [NBITS-1:0] sineLUT[0:Nsamples_LUT-1];
	//wire [N-1:0] index;
	
	initial
	begin
		$readmemh(DDSLUT,sineLUT);
	end
	
	//assign index = pplus[N+n-1:n];
	
	
	always @(posedge clock)
	begin
	if (reset)
		begin
			pplus<={18{1'b0}};
		end
	else if (enableclk)
		begin
			pplus[N+n-1:0]<=pplus[N+n-1:0]+phaseinc[N+n-1:0];
		end
		
	outsine <= sineLUT[pplus[N+n-1:n]];
	end

endmodule








