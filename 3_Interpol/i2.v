module inter(
				clock,
				clkin,
				clk4x,
				reset,
				start,
				start1,
				ready,
				ready1,
				ks,
				kd,
				left,
				right,
				LmR,
				LpR
			);

	parameter
			N=5,
			M=18;
			
	input clock;
	input reset;
	input clkin;
	input clk4x;
	input start;
	input start1;
	output ready;
	output ready1;
	input unsigned [N-2:0] ks;
	input unsigned [N-2:0] kd;
	input signed [17:0] left;
	input signed [17:0] right;
	output reg signed [M-1:0] LpR;
	output reg signed [M-1:0] LmR;
	wire signed [M+N-1:0] preLpR;
	wire signed [M+N-1:0] preLmR;
	wire signed [N-1:0] Ks;
	wire signed [N-1:0] Kd;
	reg signed [17:0] rp;
	reg signed [17:0] rm;
	wire signed [17:0] rp2;
	wire signed [17:0] rm2;
	
	wire [17:0] soma;
	wire [17:0] sub;
				
	assign soma [17:0] = left [17:0] + right [17:0];
	assign sub [17:0] = left [17:0] - right [17:0];
	
	
	assign Ks = {{1'b0},{ks}};
	assign Kd = {{1'b0},{kd}};
	//assign rp[17:0] = preLpR[M+N-3:N-2];
	//assign rm[17:0] = preLmR[M+N-3:N-2];

	always  @*
	begin
		rp[17:0]<={preLpR[M+N-3:N-2]};
		rm[17:0]<={preLmR[M+N-3:N-2]};
	end
	
	seqmultNM #(
				.N(N),
				.M(M)
	          ) 
			  seqmult_1
			  (
			  	.clock(clock),
			  	.reset(reset),
			  	.ready(ready),
			  	.start(start),
			  	.A(soma),
			  	.B(Ks),
			  	.R(preLpR)
			  );	
	
	seqmultNM #(
				.N(N),
				.M(M)
	          ) 
			  seqmult_2
			  (
			  	.clock(clock),
			  	.reset(reset),
			  	.ready(ready1),
			  	.start(start1),
			  	.A(sub),
			  	.B(Kd),
			  	.R(preLmR)
			  );
	
	interpol4x
	UUT(
			.clock(clock),
			.reset(reset),
			.clkenin(clkin),
			.clken4x(clk4x),
			.xkin(rp),
			.ykout(rp2) 
		);
	interpol4x
	UUT1(
			.clock(clock),
			.reset(reset),
			.clkenin(clkin),
			.clken4x(clk4x),
			.xkin(rm),
			.ykout(rm2) 
		);
	
	
	always @(posedge clk4x)
	begin
		LpR<=rp2[17:0];
		LmR<=rm2[17:0];
	end
	
endmodule

