`timescale 1ns/1ps

module leftright_tb;

parameter
		MAX_INPUT_SIZE = 48_000*5,

        LEFT_IN_FILENAME  = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/fmstereo_audioin_left.hex",
		RIGHT_IN_FILENAME = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/fmstereo_audioin_right.hex",
	    OUTPUT_FILENAME1    = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/LpR.hex",
		OUTPUT_FILENAME2	= "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/LmR.hex",

		CLOCKS_PER_SAMPLING_PERIOD = 2048,    // Number of clock cycles per input sampling period.
		  
		CLOCK_FREQUENCY = 48000*2048,         // Master clock frequency = 98_304_000 Hz
		  
		CLOCK_PERIOD    = 1000000000/CLOCK_FREQUENCY,  // Master clock period in nano-seconds
		  
		N_CLOCKS_RESET  = 2;                           // Number of clock cycles for applying reset
		
		
reg clock, reset;
wire	clken192kHz;
wire    clken48kHz;



integer fpout1;
integer fpout2;
integer i;
integer j;

reg signed [17:0] LEFTin,
				  RIGHTin;
	


reg signed [17:0] left_in_mem[0:MAX_INPUT_SIZE-1 + 1];
reg signed [17:0] right_in_mem[0:MAX_INPUT_SIZE-1 + 1];

wire signed [17:0] LpR;
wire signed [17:0] LmR;

clockenablegen clken48k192k( .* ); 

leftright lr(
			.left (LEFTin),
			.right(RIGHTin),
			.clock (clock),
			.LpR(LpR),
			.LmR(LmR),
			.reset(reset)
			);

initial
begin
  reset   <=  1'b0;
  clock   <=  1'b0;
  LEFTin  <= 18'd0;
  RIGHTin <= 18'd0;

end

initial
begin
  #1
  forever
  begin
    # ( CLOCK_PERIOD / 2 ) clock = 1'b0;
	# ( CLOCK_PERIOD / 2 ) clock = 1'b1;
  end
end

initial
begin
  #1
  @(negedge clock );
  reset = 1'b1;
  repeat ( N_CLOCKS_RESET )
  begin
    @(negedge clock)
    #2;
  end
  reset = 1'b0;
end

initial
begin

	fpout1 = $fopen (OUTPUT_FILENAME1, "w+");
	fpout2 = $fopen (OUTPUT_FILENAME2, "w+");

	$readmemh(LEFT_IN_FILENAME,   left_in_mem );
	$readmemh(RIGHT_IN_FILENAME,  right_in_mem );

	for(i=0; i<24000; i=i+1 )
  	begin
		#1
		@( posedge clken48kHz );
		LEFTin  = left_in_mem[i];
		RIGHTin = right_in_mem[i];
  	end
end

initial
begin
	for(j=0; j<24000; j=j+1 )
  	begin
  		@( negedge clken48kHz );
  		//@*
  		$fwrite( fpout1,"%08h\n", LpR);
  		$fwrite( fpout2,"%08h\n", LmR );
  	end
  	$fclose (fpout1);
  	$fclose (fpout2);
  	$stop ;
end
endmodule


