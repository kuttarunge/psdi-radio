`timescale 1ns / 1ps

module dds(
			clock,
			reset,
			enableclk,
			phaseinc,
			outsine
			);
			
	parameter Nsamples_LUT =128,
			  N=7,
			  n=6,
			  on=9,
			  DDSLUT ="DDSLUT.hex",
			  DDSLUTo="DDSout.hex",
			  NBITS = 32;

	
	input clock;
	input reset;
	input enableclk;
	input [NBITS-1:0] phaseinc;
	output reg [NBITS-1:0] outsine;
	
	reg [NBITS-1:0] pplus= 32'b0;
	reg [(NBITS-1):0] sineLUT[0:Nsamples_LUT-1];
	wire [N-1:0] index;
	
	initial
	begin
		$readmemh(DDSLUT,sineLUT);
	end

	always @ (posedge enableclk)
	begin
		if (reset)
			pplus={NBITS{1'b0}};
		else
			pplus[NBITS-1:0]<=pplus[NBITS-1:0]+phaseinc[NBITS-1:0];
	end
	
	assign index = pplus[N+n-1:n];
	
	always @(posedge clock)
	begin
		outsine[NBITS-1:0] <= sineLUT[index];
	end

endmodule








