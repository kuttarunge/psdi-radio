/*---------------------------------------------------------
    basic testbench for the sequential M by N multiplier

 	jca@fe.up.pt, Dec 2015
	
	This Verilog code is proprietary of University of Porto
	Its utilization beyond the scope of the course Digital Systems Design
	(Projeto de Sistemas Digitais) of the Integrated Master in Electrical 
	and Computer Engineering requires explicit authorization from the author.

*/
`timescale 1ns/1ps

module m2_tb;

// Mult parameters
parameter N  = 5; // Number of bits of the multiplier
parameter M  = 18; // Number of bits of the multiplicand

parameter
		MAX_INPUT_SIZE = 48_000*5,
        LEFT_IN_FILENAME  = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/fmstereo_audioin_left.hex",
		RIGHT_IN_FILENAME = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/fmstereo_audioin_right.hex",
	    OUTPUT_FILENAME1    = "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/LpR.hex",
		OUTPUT_FILENAME2	= "/home/cordoes/Desktop/DrawingBoard/Monsta/simdata/LmR.hex";

integer fpout1;
integer fpout2;
integer i;
integer j;
reg	a = 1'b0;
reg b = 1'b0;

reg clock;
reg reset;
reg start;
wire ready;
wire ready1;
reg signed [M-1:0] left;
reg signed [M-1:0] right;
reg unsigned [N-2:0] ks;
reg unsigned [N-2:0] kd;
wire signed [M+N-1:0] LpR2;
wire signed [M+N-1:0] LmR2;
reg signed [17:0] left_in_mem[0:MAX_INPUT_SIZE-1 + 1];
reg signed [17:0] right_in_mem[0:MAX_INPUT_SIZE-1 + 1];

multi #(
			.N(N),
			.M(M)
	   )
	   multi_1
	   (
	   		.clock(clock),
	   		.reset(reset),
	   		.start(start),
	   		.ready(ready),
	   		.ready1(ready1),
	   		.ks(ks),
	   		.kd(kd),
	   		.left(left),
	   		.right(right),
	   		.LpR(LpR2),
	   		.LmR(LmR2)
	   );

		
// Init regs and generate clock
initial
begin
  clock = 0;
  reset = 0;
  start = 0;
  ks = 0;
  left = 0;
  #3 forever #5 clock = ~clock;
end

// Apply reset:
initial
begin
  # 1;
  @(negedge clock);
  reset = 1;
  @(negedge clock);
  reset = 0;
end


// Execute one multiplication:
reg signed [M+N-1:0] expectedR;
reg signed [M+N-1:0] expectedR1; // This is needed to compute the result A x B with M+N bits
                         // in the $write( )
initial
begin
  fpout1 = $fopen (OUTPUT_FILENAME1, "w+");
    
  $readmemh(LEFT_IN_FILENAME,   left_in_mem );

  # 1;
  @(negedge reset);
  for(i=0; i<24000; i=i+1)
  begin
  	@(negedge clock);
  	ks = 4'h2;
  	left = left_in_mem[i];
  	//expectedR = ks * left;
  	//expectedR1= kd * right;
  	start = 1;
  	@(negedge clock);
  	start = 0;
  	@(negedge clock);
  	@(posedge ready);
  	#1
  	$write( "A=%d, B=%d, AxB=%d \n", ks, left, LpR2 );
  	$fwrite( fpout1,"%08h\n", LpR2);
  end
  a=1'b1;
end

initial
begin
  fpout2 = $fopen (OUTPUT_FILENAME2, "w+");
  
  $readmemh(RIGHT_IN_FILENAME,  right_in_mem );
  
  # 1;
  @(negedge reset);
  for(j=0; j<24000; j=j+1)
  begin
  	@(negedge clock);
  	kd = 4'h3;
  	right = right_in_mem[i];
  	//expectedR = ks * left;
  	//expectedR1= kd * right;
  	start = 1;
  	@(negedge clock);
  	start = 0;
  	@(negedge clock);
  	@(posedge ready);
  	#1
  	$write( "A=%d, B=%d, AxB=%d \n", kd, right, LmR2);
  	$fwrite( fpout2,"%08h\n", LmR2 );
  end
  b=1'b1;
end

always @(posedge clock)
begin
	if(b==1 & b==1)
	begin
		$stop;
	end
end
		
endmodule
