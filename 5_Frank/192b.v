`timescale 1ns / 1ps

module k192(
	input clock,
	input reset,
	input clken192kHz,
	input signed [17:0] LEFTin,
	input signed [17:0] RIGHTin,
	input [3:0] kp,
	input [7:0] kf,
	output reg [23:0] FMout
    );


	wire signed [8:0] Kf;
	wire signed [4:0] Kp;
	wire signed [7:0] outsin19;
	wire signed [7:0] outsin38;
	wire signed [25:0] RIGHT_sin38;
	wire signed [12:0] KP_sin19;
	wire signed [28:0] KF_som;
	wire readyKP;
	wire ready38;
	wire readyKF;
	reg  startKP;
	reg  start38;
	reg  startKF;
	reg signed [17:0] RIGHT_sin38S;
	reg signed [23:0] KF_somS;
	reg signed [17:0] KF_sin19S;
	wire signed [19:0] out_sum;
	reg fsin19;
	reg fsin38;

	seqmultNM #(
				.M(8), 
				.N(5)
			   ) 
		seqmult_3( 
		.clock(clock),
		.reset(reset),
		.start(startKP),
		.ready(readyKP),
		.A(outsin19),
		.B(Kp),
		.R(KP_sin19)
	);

	seqmultNM #(
				.M(18), 
				.N(8)
				) 
		seqmult_4( 
		.clock(clock),
		.reset(reset),
		.start(start38),
		.ready(ready38),
		.A(RIGHTin),
		.B(outsin38),
		.R(RIGHT_sin38)
	);

	seqmultNM #(
				.M(20),
				.N(9)
			   )
		seqmult_5( 
		.clock(clock),
		.reset(reset),
		.start(startKF),
		.ready(readyKF),
		.A(out_sum),
		.B(Kf),
		.R(KF_som)
	);


	dds #(
		   .DDSLUT("DDSLUT_38.hex")
		 ) 
		 dds1( 
		.clock(clock),
		.reset(reset),
		.enableclk(clken192kHz),
		.phaseinc(18'b001100_101010101010),
		.outsine(outsin38)
	);

	dds #(
		   .DDSLUT("DDSLUT_19.hex")
		 ) 
		 dds2
		 ( 
		.clock(clock),
		.reset(reset),
		.enableclk(clken192kHz),
		.phaseinc(18'b000110_010101010101),
		.outsine(outsin19)
	);




	assign Kp = {1'b0, kp};
	assign Kf = {1'b0, kf};


	assign out_sum = (LEFTin + KF_sin19S + RIGHT_sin38S);

	always @(posedge clock)
	begin

		if (reset)
		begin
		  FMout <= 0;
		  startKP <= 0;
		  fsin19 <= 0;
		  KF_sin19S <= 0;  
		  start38 <= 0;
		  fsin38 <= 0;
		  RIGHT_sin38S <= 0;  
		  startKF <= 0;
		  KF_somS <= 0;
		end

		if (clken192kHz && ~(fsin19))
		begin
		  startKP <= 1;
		end
		else if(startKP && ~(readyKP))
		begin
		  startKP <= 0;
		end

		if (clken192kHz && ~(fsin38))
		begin
		  start38 <= 1;
		end
		else if(start38 && ~(ready38))
		begin
		  start38 <= 0;
		end
								
		if(fsin38 && fsin19)
		begin
		  startKF <= 1;
		  fsin38 <= 0;
		  fsin19 <= 0;
		end
		else if(startKF && ~(readyKF))
		begin
		  startKF <= 0;
		end
	end 
	
	always @(posedge clken192kHz)
	begin
		FMout <= KF_somS;
	end
	
	always @(posedge readyKP)
	begin
		KF_sin19S <= KP_sin19 << 6;
		fsin19 <= 1;
	end
	
	always @(posedge ready38)
	begin
		RIGHT_sin38S <= RIGHT_sin38[25:8];
		fsin38 <= 1;	
	end
	
	always @(posedge readyKF)
	begin
		KF_somS <= KF_som >>> 4;
	end
endmodule







