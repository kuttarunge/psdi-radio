`timescale 1ns / 1ps


module finalbeast(
    
    input clock,
    input reset,
    input signed [17:0] LEFTin,
    input signed [17:0] RIGHTin,
    input [3:0] Ks,
    input [3:0] Kd,
    input [3:0] Kp,
    input [7:0] Kf,
    input clken48kHz,
    input clken192kHz,
    output signed [23:0] FMout
);



	wire [17:0] firstp;
	wire [17:0] firstm;


	wire [17:0] secondp;
	wire [17:0] secondm;

	k48 k48(
		.clock(clock),
		.reset(reset),
		.LEFTin(LEFTin),
		.RIGHTin(RIGHTin),
		.ks(Ks),
		.kd(Kd),
		.clken48kHz(clken48kHz),
		.outLpR(firstp),
		.outLmR(firstm)
	);

	k192 k192(
		.clock(clock),
		.reset(reset),
		.clken192kHz(clken192kHz),
		.LEFTin(secondp),
		.RIGHTin(secondm),
		.kp(Kp),
		.kf(Kf),
		.FMout(FMout)
	);
		
	interpol4x interpol4x_LpR(
		.clock(clock),
		.reset(reset),
		.clkenin(clken48kHz),
		.clken4x(clken192kHz),
		.xkin(firstp),
		.ykout(secondp)
	);

	interpol4x interpol4x_LmR(
		.clock(clock),
		.reset(reset),
		.clkenin(clken48kHz),
		.clken4x(clken192kHz),
		.xkin(firstm),
		.ykout(secondm)
	);

endmodule
