`timescale 1ns/1ps

module inter_tb;

parameter INPUT_FILENAME    = "/home/cordoes/Desktop/DrawingBoard/3_Inter/Data/fmstereo_audioin_left.hex",
		  INPUT_FILENAME1   = "/home/cordoes/Desktop/DrawingBoard/3_Inter/Data/fmstereo_audioin_right.hex",
          OUTGOLDEN_FILENAME = "/home/cordoes/Desktop/DrawingBoard/3_Inter/Data/LpR.hex",
          OUTGOLDEN_FILENAME1 = "/home/cordoes/Desktop/DrawingBoard/3_Inter/Data/LmR.hex",
          MAXPOS             = 24000,   // Maximum number of data samples to load
		  MAXERRORS          = 100,     // Max errors to abort simulation
		  CLOCK_PERIOD       = 8.138;  // Clock period in ns (Fclk = 12.288 MHz)

parameter
		N=5,
		M=18;
		
reg  signed [17:0] leftV[0:MAXPOS];
reg  signed [17:0] rightV[0:MAXPOS];
//reg  signed [17:0] LpRV[0:MAXPOS];	

reg clock;
reg start;
reg start1;
wire ready;
wire ready1;
reg reset;
reg en48000Hz;
reg en192000Hz;

reg unsigned [N-2:0] ks;
reg unsigned [N-2:0] kd;
reg  signed [17:0] left;  // input signal
reg  signed [17:0] right;  // input signal
wire signed [17:0] LpR;  // output signal
wire signed [17:0] LmR;  // output signal

integer i;
integer ni;
integer j;
integer fpout2;
integer fpout1;

reg	a = 1'b0;
reg b = 1'b0;

inter 
UUT( 
			.start(start),
			.start1(start1),
			.ready(ready),
			.ready1(ready1),
			.ks(ks),
			.kd(kd),
            .clock( clock ), 
            .reset( reset ),
		    .clkin( en48000Hz ),
            .clk4x( en192000Hz ),
		    .left( left ),
		    .right(right),
		    .LmR( LmR ),
		    .LpR( LpR )
		 );

initial
begin
	$readmemh (INPUT_FILENAME, leftV);
	$readmemh (INPUT_FILENAME1, rightV);
	fpout2 = $fopen (OUTGOLDEN_FILENAME, "w+");
	fpout1 = $fopen (OUTGOLDEN_FILENAME1, "w+");
	
	ni = 0;
  	while ( leftV[ ni ] !== 18'hx )
  	begin
    	ni = ni + 1;
  	end
end

initial
begin
	start = 1'b0;
	start1 =1'b0;
	clock = 1'b0;
	reset = 1'b0;
	left = 18'b0;
	right= 18'b0;
	en48000Hz = 1'b0;
	en192000Hz = 1'b0;
	#104
	forever #4.069 clock=~clock;
end

initial
begin
	#200
	reset=1;
	repeat(10)
		@(negedge clock);
	reset=0;
end

always 
begin
  #1
  @(negedge reset);
  #1
  repeat (10)
    @(negedge clock);
  #1
  while (1)
  begin
    en192000Hz = 1;
    @(negedge clock);
    #2
    en192000Hz = 0;
    repeat (63)
      @(negedge clock);
  end
end

initial 
begin
  #1
  @(negedge reset);
  #1
  repeat (10)
    @(negedge clock);
  #1
  while (1)
  begin
    en48000Hz = 1;
    @(negedge clock);
    #2
    en48000Hz = 0;
    repeat (255)
      @(negedge clock);
  end
end

initial
begin
	@(negedge reset);
	for(i=0;i<ni;i=i+1)
	begin
		@(posedge en48000Hz)
		ks = 4'h2;
		left = leftV[i];
		start = 1;
  		@(negedge clock);
  		start = 0;
	end
	repeat(9)
		 @(posedge en192000Hz);
	$fclose (fpout2);
	a=1'b1;
end

initial
begin

	@(negedge reset);
	for(j=0;j<ni;j=j+1)
	begin
		@(posedge en48000Hz)
		kd=4'h8;
		right = rightV[j];			//CUIDADO isto vai ser rightV
		start1 = 1;
  		@(negedge clock);
  		start1 = 0;
	end
	repeat(9)
		 @(posedge en192000Hz);
	$fclose (fpout1);
	b=1'b1;
end

initial
begin
	@(posedge ready);
	while(1)
	begin
		@(negedge en192000Hz);
		@(posedge clock);
		$display("LpR = %d", LpR);
		$fwrite( fpout2,"%08h\n", LpR );	
	end
end

initial
begin
	@(posedge ready1);
	while(1)
	begin
		@(negedge en192000Hz);
		@(posedge clock);
		$display("LmR = %d", LmR);
		$fwrite( fpout1,"%08h\n", LmR );
	end
end

always @(posedge clock)
begin
	if(a==1 & b==1)
	begin
		$stop;
	end
end
endmodule













