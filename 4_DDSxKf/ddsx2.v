`timescale 1ns/1ps

module ddsx2 (
					clock,
					reset,
					enableclk,
					phaseinc,
					outsine,
					kf
			 );

	parameter DDSLUT = "DDSLUT.hex",
			  NBITS = 32,
			  N=7,
			  n=6,
			  on=9,
			  Nsamples_LUT=128,
			  mier=5,
			  mand=32;
			
	integer fpout1;						
	input clock;
	input reset;
	input enableclk;
	input [NBITS-1:0] phaseinc;
	input unsigned [mier-2:0] kf;
	output reg signed [mand+mier-1:0] outsine;
	wire signed [31:0] LpR;
	reg start;
	wire ready;
	reg signed [mier-1:0] B;
	reg signed [mand-1:0] A;
	wire signed [mand+mier-1:0] R;
	integer i;
	wire signed [mier-1:0] Kf;
	
	assign Kf ={{1'b0},{kf}};
	
	dds #(.Nsamples_LUT(128),
		  .NBITS(32),
		  .n(6),
		  .DDSLUT("DDSLUT.hex")
		  )  
		  dds_1( 
		        .clock( clock ),
				.reset( reset ),
				.enableclk( enableclk ),
				.phaseinc( phaseinc ),
				.outsine( LpR )
				);
	

	seqmultNM #( 
              .N( mier ), // Number of bits of the multiplier
			  .M( mand )  // Number of bits of the multiplicand
		    )
			seqmult_1
       (
			.clock( clock ),
			.reset( reset ),
			.start( start ), // input, set start=1 during one clock cycle to start the multiplication
			.ready( ready ), // output, set to 1 when the multiplier is ready to accept a new start
			                 //         and the result is ready after activating input start
			.A( A ),         // Multiplicand,  M bits
			.B( B ),         // Multiplier,    N bits
			.R( R )          // Result: A x B, M+N bits
		);	
	
	
	

	initial
	begin 
		fpout1 = $fopen ("DDSx2.hex", "w+");
		start<=0;
		A<=0;
		B<=0;
	end
	
	always @(clock)
	begin
		A=LpR;
		B=Kf;
		start=1;
		@(negedge clock);
		start=0;
		@(posedge ready);
		outsine<=R;
		$fwrite( fpout1,"%08h\n", LpR );
	end

/*
	always @(posedge clock)
	begin
		outsine<=R;
	end
		
	initial
	begin
		@(negedge reset);
		for(i=0; i<19200; i=i+1)
		begin
			@(negedge clock);
			A=32'b0;
			B=5'b00010;
			start=1;
			@(negedge clock);
			start=0;
			@(posedge ready);
			outsine<=R;
			$fwrite( fpout1,"%08h\n", R );
		end
	end
	
*/
	
endmodule	
